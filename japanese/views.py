#from django.shortcuts import render
from django.views.generic import TemplateView

class HiraganaPracticeView(TemplateView):
    template_name = "japanese/hiragana.html"


class KatakanaPracticeView(TemplateView):
    template_name = "japanese/katakana.html"
