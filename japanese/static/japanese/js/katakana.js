
/**
 * Returns detailed type as string (instead of just 'object' for arrays etc)
 * @private
 * @param {any} value js value
 * @returns {String} type of value
 * @example
 * typeOf({}); // 'object'
 * typeOf([]); // 'array'
 * typeOf(function() {}); // 'function'
 * typeOf(/a/); // 'regexp'
 * typeOf(new Date()); // 'date'
 * typeOf(null); // 'null'
 * typeOf(undefined); // 'undefined'
 * typeOf('a'); // 'string'
 * typeOf(1); // 'number'
 * typeOf(true); // 'boolean'
 * typeOf(new Map()); // 'map'
 * typeOf(new Set()); // 'map'
 */
function typeOf(value) {
    if (value === null) {
    return 'null';
    }
    if (value !== Object(value)) {
    return typeof value;
    }
    return {}.toString.call(value).slice(8, -1).toLowerCase();
}

/**
 * Checks if input string is empty
 * @param  {String} input text input
 * @return {Boolean} true if no input
 */
function isEmpty(input) {
    if (typeOf(input) !== 'string') {
    return true;
    }
    return !input.length;
}

/**
 * Takes a character and a unicode range. Returns true if the char is in the range.
 * @param  {String}  char  unicode character
 * @param  {Number}  start unicode start range
 * @param  {Number}  end   unicode end range
 * @return {Boolean}
 */
function isCharInRange(char = '', start, end) {
    if (isEmpty(char)) return false;
    const code = char.charCodeAt(0);
    return start <= code && code <= end;
}

const TO_KANA_METHODS = {
    HIRAGANA: 'toHiragana',
    KATAKANA: 'toKatakana'
};

const ROMANIZATIONS = {
    HEPBURN: 'hepburn'
};

/**
 * Default config for WanaKana, user passed options will be merged with these
 * @type {DefaultOptions}
 * @name defaultOptions
 * @property {Boolean} [useObsoleteKana=false] - Set to true to use obsolete characters, such as ゐ and ゑ.
 * @example
 * toHiragana('we', { useObsoleteKana: true })
 * // => 'ゑ'
 * @property {Boolean} [passRomaji=false] - Set to true to pass romaji when using mixed syllabaries with toKatakana() or toHiragana()
 * @example
 * toHiragana('only convert the katakana: ヒラガナ', { passRomaji: true })
 * // => "only convert the katakana: ヒラガナ"
 * @property {Boolean} [upcaseKatakana=false] - Set to true to convert katakana to uppercase using toRomaji()
 * @example
 * toRomaji('ヒラガナ カタカナ', { upcaseKatakana: true })
 * // => "hiragana KATAKANA"
 * @property {Boolean|String} [IMEMode=false] - Set to true, 'toHiragana', or 'toKatakana' to handle conversion while it is being typed.
 * @property {String} [romanization='hepburn'] - choose toRomaji() romanization map (currently only 'hepburn')
 * @property {Object} [customKanaMapping] - custom map will be merged with default conversion
 * @example
 * toKana('wanakana', { customKanaMapping: { na: 'ニ', ka: 'Bana' }) };
 * // => 'ワニBanaニ'
 * @property {Object} [customRomajiMapping] - custom map will be merged with default conversion
 * @example
 * toRomaji('ツジギリ', { customRomajiMapping: { ジ: 'zi', ツ: 'tu', リ: 'li' }) };
 * // => 'tuzigili'
 */
const DEFAULT_OPTIONS = {
    useObsoleteKana: false,
    passRomaji: false,
    upcaseKatakana: false,
    ignoreCase: false,
    IMEMode: false,
    romanization: ROMANIZATIONS.HEPBURN
};
const LATIN_UPPERCASE_START = 0x41;
const LATIN_UPPERCASE_END = 0x5a;
const HIRAGANA_START = 0x3041;
const HIRAGANA_END = 0x3096;
const KATAKANA_START = 0x30a1;
const KATAKANA_END = 0x30fc;
const KANJI_START = 0x4e00;
const KANJI_END = 0x9faf;
const PROLONGED_SOUND_MARK = 0x30fc;
const KANA_SLASH_DOT = 0x30fb;

const MODERN_ENGLISH = [0x0000, 0x007f];
const HEPBURN_MACRON_RANGES = [[0x0100, 0x0101], // Ā ā
[0x0112, 0x0113], // Ē ē
[0x012a, 0x012b], // Ī ī
[0x014c, 0x014d], // Ō ō
[0x016a, 0x016b]];
const SMART_QUOTE_RANGES = [[0x2018, 0x2019], // ‘ ’
[0x201c, 0x201d]];

const ROMAJI_RANGES = [MODERN_ENGLISH, ...HEPBURN_MACRON_RANGES];

const EN_PUNCTUATION_RANGES = [[0x20, 0x2f], [0x3a, 0x3f], [0x5b, 0x60], [0x7b, 0x7e], ...SMART_QUOTE_RANGES];

/**
 * Easy re-use of merging with default options
 * @param {Object} opts user options
 * @returns user options merged over default options
 */
const mergeWithDefaultOptions = (opts = {}) => Object.assign({}, DEFAULT_OPTIONS, opts);

function applyMapping(string, mapping, convertEnding) {
    const root = mapping;

    function nextSubtree(tree, nextChar) {
    const subtree = tree[nextChar];
    if (subtree === undefined) {
        return undefined;
    }
    // if the next child node does not have a node value, set its node value to the input
    return Object.assign({ '': tree[''] + nextChar }, tree[nextChar]);
    }

    function newChunk(remaining, currentCursor) {
    // start parsing a new chunk
    const firstChar = remaining.charAt(0);

    return parse(Object.assign({ '': firstChar }, root[firstChar]), remaining.slice(1), currentCursor, currentCursor + 1);
    }

    function parse(tree, remaining, lastCursor, currentCursor) {
    if (!remaining) {
        if (convertEnding || Object.keys(tree).length === 1) {
        // nothing more to consume, just commit the last chunk and return it
        // so as to not have an empty element at the end of the result
        return tree[''] ? [[lastCursor, currentCursor, tree['']]] : [];
        }
        // if we don't want to convert the ending, because there are still possible continuations
        // return null as the final node value
        return [[lastCursor, currentCursor, null]];
    }

    if (Object.keys(tree).length === 1) {
        return [[lastCursor, currentCursor, tree['']]].concat(newChunk(remaining, currentCursor));
    }

    const subtree = nextSubtree(tree, remaining.charAt(0));

    if (subtree === undefined) {
        return [[lastCursor, currentCursor, tree['']]].concat(newChunk(remaining, currentCursor));
    }
    // continue current branch
    return parse(subtree, remaining.slice(1), lastCursor, currentCursor + 1);
    }

    return newChunk(string, 0);
}

// transform the tree, so that for example hepburnTree['ゔ']['ぁ'][''] === 'va'
// or kanaTree['k']['y']['a'][''] === 'キャ'
function transform(tree) {
    return Object.entries(tree).reduce((map, [char, subtree]) => {
    const endOfBranch = typeOf(subtree) === 'string';
    map[char] = endOfBranch ? { '': subtree } : transform(subtree);
    return map;
    }, {});
}

function getSubTreeOf(tree, string) {
    return string.split('').reduce((correctSubTree, char) => {
    if (correctSubTree[char] === undefined) {
        correctSubTree[char] = {};
    }
    return correctSubTree[char];
    }, tree);
}

/**
 * Creates a custom mapping tree, returns a function that accepts a defaultMap which the newly created customMapping will be merged with and returned
 * (customMap) => (defaultMap) => mergedMap
 * @param  {Object} customMap { 'ka' : 'ナ' }
 * @return {Function} (defaultMap) => defaultMergedWithCustomMap
 * @example
 * const sillyMap = createCustomMapping({ 'チャ': 'time', '茎': 'cookie'　});
 * // sillyMap is passed defaultMapping to merge with when called in toRomaji()
 * toRomaji("It's 茎 チャ ヨ", { customRomajiMapping: sillyMap });
 * // => 'It's cookie time yo';
 */
function createCustomMapping(customMap = {}) {
    const customTree = {};

    if (typeOf(customMap) === 'object') {
    Object.entries(customMap).forEach(([roma, kana]) => {
        let subTree = customTree;
        roma.split('').forEach(char => {
        if (subTree[char] === undefined) {
            subTree[char] = {};
        }
        subTree = subTree[char];
        });
        subTree[''] = kana;
    });
    }

    return function makeMap(map) {
    const mapCopy = JSON.parse(JSON.stringify(map));

    function transformMap(mapSubtree, customSubtree) {
        if (mapSubtree === undefined || typeOf(mapSubtree) === 'string') {
        return customSubtree;
        }
        return Object.entries(customSubtree).reduce((newSubtree, [char, subtree]) => {
        newSubtree[char] = transformMap(mapSubtree[char], subtree);
        return newSubtree;
        }, mapSubtree);
    }

    return transformMap(mapCopy, customTree);
    };
}

// allow consumer to pass either function or object as customMapping
function mergeCustomMapping(map, customMapping) {
    if (!customMapping) {
    return map;
    }
    return typeOf(customMapping) === 'function' ? customMapping(map) : createCustomMapping(customMapping)(map);
}

// NOTE: not exactly kunrei shiki, for example ヂャ -> dya instead of zya, to avoid name clashing
/* eslint-disable */
// prettier-ignore
const BASIC_KUNREI = {
    a: 'ア', i: 'イ', u: 'ウ', e: 'エ', o: 'オ',
    k: { a: 'カ', i: 'キ', u: 'ク', e: 'ケ', o: 'コ' },
    s: { a: 'サ', i: 'シ', u: 'ス', e: 'セ', o: 'ソ' },
    t: { a: 'タ', i: 'チ', u: 'ツ', e: 'テ', o: 'ト' },
    n: { a: 'ナ', i: 'ニ', u: 'ヌ', e: 'ネ', o: 'ノ' },
    h: { a: 'ハ', i: 'ヒ', u: 'フ', e: 'ヘ', o: 'ホ' },
    m: { a: 'マ', i: 'ミ', u: 'ム', e: 'メ', o: 'モ' },
    y: { a: 'ヤ', u: 'ユ', o: 'ヨ' },
    r: { a: 'ラ', i: 'リ', u: 'ル', e: 'レ', o: 'ロ' },
    w: { a: 'ワ', i: 'ゐ', e: 'ゑ', o: 'ヲ' },
    g: { a: 'ガ', i: 'ギ', u: 'グ', e: 'ゲ', o: 'ゴ' },
    z: { a: 'ザ', i: 'ジ', u: 'ズ', e: 'ゼ', o: 'ゾ' },
    d: { a: 'ダ', i: 'ヂ', u: 'ヅ', e: 'デ', o: 'ド' },
    b: { a: 'バ', i: 'ビ', u: 'ブ', e: 'ベ', o: 'ボ' },
    p: { a: 'パ', i: 'ピ', u: 'プ', e: 'ペ', o: 'ポ' },
    v: { a: 'ゔぁ', i: 'ゔぃ', u: 'ゔ', e: 'ゔぇ', o: 'ゔぉ' }
};

const SPECIAL_SYMBOLS = {
    '.': '。',
    ',': '、',
    ':': '：',
    '/': '・',
    '!': '！',
    '?': '？',
    '~': '〜',
    '-': 'ー',
    '‘': '「',
    '’': '」',
    '“': '『',
    '”': '』',
    '[': '［',
    ']': '］',
    '(': '（',
    ')': '）',
    '{': '｛',
    '}': '｝'
};

const CONSONANTS = {
    k: 'キ',
    s: 'シ',
    t: 'チ',
    n: 'ニ',
    h: 'ヒ',
    m: 'ミ',
    r: 'リ',
    g: 'ギ',
    z: 'ジ',
    d: 'ヂ',
    b: 'ビ',
    p: 'ピ',
    v: 'ゔ',
    q: 'ク',
    f: 'フ'
};
const SMALL_Y = { ya: 'ャ', yi: 'ぃ', yu: 'ュ', ye: 'ぇ', yo: 'ョ' };
const SMALL_VOWELS = { a: 'ぁ', i: 'ぃ', u: 'ぅ', e: 'ぇ', o: 'ぉ' };

// typing one should be the same as having typed the other instead
const ALIASES = {
    sh: 'sy', // sha -> sya
    ch: 'ty', // cho -> tyo
    cy: 'ty', // cyo -> tyo
    chy: 'ty', // chyu -> tyu
    shy: 'sy', // shya -> sya
    j: 'zy', // ja -> zya
    jy: 'zy', // jye -> zye

    // exceptions to above rules
    shi: 'si',
    chi: 'ti',
    tsu: 'tu',
    ji: 'zi',
    fu: 'hu'
};

// xtu -> っ
const SMALL_LETTERS = Object.assign({
    tu: 'っ',
    wa: 'ゎ',
    ka: 'ヵ',
    ke: 'ヶ'
}, SMALL_VOWELS, SMALL_Y);

// don't follow any notable patterns
const SPECIAL_CASES = {
    yi: 'イ',
    wu: 'ウ',
    ye: 'イぇ',
    wi: 'ウぃ',
    we: 'ウぇ',
    kwa: 'クぁ',
    whu: 'ウ',
    // because it's not thya for テャ but tha
    // and tha is not テぁ, but テャ
    tha: 'テャ',
    thu: 'テュ',
    tho: 'テョ',
    dha: 'デャ',
    dhu: 'デュ',
    dho: 'デョ'
};

const AIUEO_CONSTRUCTIONS = {
    wh: 'ウ',
    qw: 'ク',
    q: 'ク',
    gw: 'グ',
    sw: 'ス',
    ts: 'ツ',
    th: 'テ',
    tw: 'ト',
    dh: 'デ',
    dw: 'ド',
    fw: 'フ',
    f: 'フ'
};

/* eslint-enable */
function createRomajiToKanaMap() {
    const kanaTree = transform(BASIC_KUNREI);
    // pseudo partial application
    const subtreeOf = string => getSubTreeOf(kanaTree, string);

    // add tya, sya, etc.
    Object.entries(CONSONANTS).forEach(([consonant, yKana]) => {
    Object.entries(SMALL_Y).forEach(([roma, kana]) => {
        // for example kyo -> キ + ョ
        subtreeOf(consonant + roma)[''] = yKana + kana;
    });
    });

    Object.entries(SPECIAL_SYMBOLS).forEach(([symbol, jsymbol]) => {
    subtreeOf(symbol)[''] = jsymbol;
    });

    // things like ウぃ, クぃ, etc.
    Object.entries(AIUEO_CONSTRUCTIONS).forEach(([consonant, aiueoKana]) => {
    Object.entries(SMALL_VOWELS).forEach(([vowel, kana]) => {
        const subtree = subtreeOf(consonant + vowel);
        subtree[''] = aiueoKana + kana;
    });
    });

    // different ways to write ン
    ['n', "n'", 'xn'].forEach(nChar => {
    subtreeOf(nChar)[''] = 'ン';
    });

    // c is equivalent to k, but not for chi, cha, etc. that's why we have to make a copy of k
    kanaTree.c = JSON.parse(JSON.stringify(kanaTree.k));

    Object.entries(ALIASES).forEach(([string, alternative]) => {
    const allExceptLast = string.slice(0, string.length - 1);
    const last = string.charAt(string.length - 1);
    const parentTree = subtreeOf(allExceptLast);
    // copy to avoid recursive containment
    parentTree[last] = JSON.parse(JSON.stringify(subtreeOf(alternative)));
    });

    function getAlternatives(string) {
    return [...Object.entries(ALIASES), ...[['c', 'k']]].reduce((list, [alt, roma]) => string.startsWith(roma) ? list.concat(string.replace(roma, alt)) : list, []);
    }

    Object.entries(SMALL_LETTERS).forEach(([kunreiRoma, kana]) => {
    const last = char => char.charAt(char.length - 1);
    const allExceptLast = chars => chars.slice(0, chars.length - 1);
    const xRoma = `x${kunreiRoma}`;
    const xSubtree = subtreeOf(xRoma);
    xSubtree[''] = kana;

    // ltu -> xtu -> っ
    const parentTree = subtreeOf(`l${allExceptLast(kunreiRoma)}`);
    parentTree[last(kunreiRoma)] = xSubtree;

    // ltsu -> ltu -> っ
    getAlternatives(kunreiRoma).forEach(altRoma => {
        ['l', 'x'].forEach(prefix => {
        const altParentTree = subtreeOf(prefix + allExceptLast(altRoma));
        altParentTree[last(altRoma)] = subtreeOf(prefix + kunreiRoma);
        });
    });
    });

    Object.entries(SPECIAL_CASES).forEach(([string, kana]) => {
    subtreeOf(string)[''] = kana;
    });

    // add kka, tta, etc.
    function addTsu(tree) {
    return Object.entries(tree).reduce((tsuTree, [key, value]) => {
        if (!key) {
        // we have reached the bottom of this branch
        tsuTree[key] = `っ${value}`;
        } else {
        // more subtrees
        tsuTree[key] = addTsu(value);
        }
        return tsuTree;
    }, {});
    }
    // have to explicitly name c here, because we made it a copy of k, not a reference
    [...Object.keys(CONSONANTS), 'c', 'y', 'w', 'j'].forEach(consonant => {
    const subtree = kanaTree[consonant];
    subtree[consonant] = addTsu(subtree);
    });
    // nn should not be っン
    delete kanaTree.n.n;
    // solidify the results, so that there there is referential transparency within the tree
    return Object.freeze(JSON.parse(JSON.stringify(kanaTree)));
}

let romajiToKanaMap = null;

function getRomajiToKanaTree() {
    if (romajiToKanaMap == null) {
    romajiToKanaMap = createRomajiToKanaMap();
    }
    return romajiToKanaMap;
}

const USE_OBSOLETE_KANA_MAP = createCustomMapping({ wi: 'ゐ', we: 'ゑ' });

function IME_MODE_MAP(map) {
    // in IME mode, we do not want to convert single ns
    const mapCopy = JSON.parse(JSON.stringify(map));
    mapCopy.n.n = { '': 'ン' };
    mapCopy.n[' '] = { '': 'ン' };
    return mapCopy;
}

/**
 * Tests if char is in English unicode uppercase range
 * @param  {String} char
 * @return {Boolean}
 */
function isCharUpperCase(char = '') {
    if (isEmpty(char)) return false;
    return isCharInRange(char, LATIN_UPPERCASE_START, LATIN_UPPERCASE_END);
}

/**
 * Returns true if char is 'ー'
 * @param  {String} char to test
 * @return {Boolean}
 */
function isCharLongDash(char = '') {
    if (isEmpty(char)) return false;
    return char.charCodeAt(0) === PROLONGED_SOUND_MARK;
}

/**
 * Tests if char is '・'
 * @param  {String} char
 * @return {Boolean} true if '・'
 */
function isCharSlashDot(char = '') {
    if (isEmpty(char)) return false;
    return char.charCodeAt(0) === KANA_SLASH_DOT;
}

/**
 * Tests a character. Returns true if the character is [Hiragana](https://en.wikipedia.org/wiki/Hiragana).
 * @param  {String} char character string to test
 * @return {Boolean}
 */
function isCharHiragana(char = '') {
    if (isEmpty(char)) return false;
    if (isCharLongDash(char)) return true;
    return isCharInRange(char, HIRAGANA_START, HIRAGANA_END);
}

/**
 * Convert [Hiragana](https://en.wikipedia.org/wiki/Hiragana) to [Katakana](https://en.wikipedia.org/wiki/Katakana)
 * Passes through any non-hiragana chars
 * @private
 * @param  {String} [input=''] text input
 * @return {String} converted text
 * @example
 * hiraganaToKatakana('ヒラガナ')
 * // => "ヒラガナ"
 * hiraganaToKatakana('ヒラガナ is a type of kana')
 * // => "ヒラガナ is a type of kana"
 */
function hiraganaToKatakana(input = '') {
    const kata = [];
    input.split('').forEach(char => {
    // Short circuit to avoid incorrect codeshift for 'ー' and '・'
    if (isCharLongDash(char) || isCharSlashDot(char)) {
        kata.push(char);
    } else if (isCharHiragana(char)) {
        // Shift charcode.
        const code = char.charCodeAt(0) + (KATAKANA_START - HIRAGANA_START);
        const kataChar = String.fromCharCode(code);
        kata.push(kataChar);
    } else {
        // Pass non-hiragana chars through
        kata.push(char);
    }
    });
    return kata.join('');
}

/**
 * Convert [Romaji](https://en.wikipedia.org/wiki/Romaji) to [Kana](https://en.wikipedia.org/wiki/Kana), lowercase text will result in [Hiragana](https://en.wikipedia.org/wiki/Hiragana) and uppercase text will result in [Katakana](https://en.wikipedia.org/wiki/Katakana).
 * @param  {String} [input=''] text
 * @param  {DefaultOptions} [options=defaultOptions]
 * @return {String} converted text
 * @example
 * toKana('onaji BUTTSUUJI')
 * // => 'オナジ ブッツウジ'
 * toKana('ONAJI buttsuuji')
 * // => 'オナジ ブっツウジ'
 * toKana('座禅‘zazen’スタイル')
 * // => '座禅「ザゼン」スタイル'
 * toKana('batsuge-mu')
 * // => 'バツゲーム'
 * toKana('!?.:/,~-‘’“”[](){}') // Punctuation conversion
 * // => '！？。：・、〜ー「」『』［］（）｛｝'
 * toKana('we', { useObsoleteKana: true })
 * // => 'ゑ'
 * toKana('wanakana', { customKanaMapping: { na: 'ニ', ka: 'bana' } });
 * // => 'ワニbanaニ'
 */
function toKana(input = '', options = {}, map) {
    let config;
    if (!map) {
    config = mergeWithDefaultOptions(options);
    map = createRomajiToKanaMap$1(config);
    } else {
    config = options;
    }

    // throw away the substring index information and just concatenate all the kana
    return splitIntoConvertedKana(input, config, map).map(kanaToken => {
    const [start, end, kana] = kanaToken;
    if (kana === null) {
        // haven't converted the end of the string, since we are in IME mode
        return input.slice(start);
    }
    const enforceHiragana = config.IMEMode === TO_KANA_METHODS.HIRAGANA;
    const enforceKatakana = config.IMEMode === TO_KANA_METHODS.KATAKANA || [...input.slice(start, end)].every(isCharUpperCase);

    return enforceHiragana || !enforceKatakana ? kana : hiraganaToKatakana(kana);
    }).join('');
}

/**
 *
 * @private
 * @param {String} [input=''] input text
 * @param {Object} [options={}] toKana options
 * @returns {Array[]} [[start, end, token]]
 * @example
 * splitIntoConvertedKana('buttsuuji')
 * // => [[0, 2, 'ブ'], [2, 6, 'っツ'], [6, 7, 'ウ'], [7, 9, 'ジ']]
 */
function splitIntoConvertedKana(input = '', options = {}, map) {
    if (!map) {
    map = createRomajiToKanaMap$1(options);
    }
    return applyMapping(input.toLowerCase(), map, !options.IMEMode);
}

let customMapping = null;
function createRomajiToKanaMap$1(options = {}) {
    let map = getRomajiToKanaTree();

    map = options.IMEMode ? IME_MODE_MAP(map) : map;
    map = options.useObsoleteKana ? USE_OBSOLETE_KANA_MAP(map) : map;

    if (options.customKanaMapping) {
    if (customMapping == null) {
        customMapping = mergeCustomMapping(map, options.customKanaMapping);
    }
    map = customMapping;
    }

    return map;
}

/**
 * Tests a character. Returns true if the character is [Romaji](https://en.wikipedia.org/wiki/Romaji) (allowing [Hepburn romanisation](https://en.wikipedia.org/wiki/Hepburn_romanization))
 * @param  {String} char character string to test
 * @return {Boolean}
 */
function isCharRomaji(char = '') {
    if (isEmpty(char)) return false;
    return ROMAJI_RANGES.some(([start, end]) => isCharInRange(char, start, end));
}

/**
 * Test if `input` is [Romaji](https://en.wikipedia.org/wiki/Romaji) (allowing [Hepburn romanisation](https://en.wikipedia.org/wiki/Hepburn_romanization))
 * @param  {String} [input=''] text
 * @param  {Regexp} [allowed] additional test allowed to pass for each char
 * @return {Boolean} true if [Romaji](https://en.wikipedia.org/wiki/Romaji)
 * @example
 * isRomaji('Tōkyō and Ōsaka')
 * // => true
 * isRomaji('12a*b&c-d')
 * // => true
 * isRomaji('アアA')
 * // => false
 * isRomaji('オ願イ')
 * // => false
 * isRomaji('a！b&cーd') // Zenkaku punctuation fails
 * // => false
 * isRomaji('a！b&cーd', /[！ー]/)
 * // => true
 */
function isRomaji(input = '', allowed) {
    const augmented = typeOf(allowed) === 'regexp';
    return isEmpty(input) ? false : [...input].every(char => {
    const isRoma = isCharRomaji(char);
    return !augmented ? isRoma : isRoma || allowed.test(char);
    });
}

/**
 * Tests a character. Returns true if the character is [Katakana](https://en.wikipedia.org/wiki/Katakana).
 * @param  {String} char character string to test
 * @return {Boolean}
 */
function isCharKatakana(char = '') {
    return isCharInRange(char, KATAKANA_START, KATAKANA_END);
}

/**
 * Test if `input` is [Hiragana](https://en.wikipedia.org/wiki/Hiragana)
 * @param  {String} [input=''] text
 * @return {Boolean} true if all [Hiragana](https://en.wikipedia.org/wiki/Hiragana)
 * @example
 * isHiragana('ゲーム')
 * // => true
 * isHiragana('A')
 * // => false
 * isHiragana('アア')
 * // => false
 */
function isHiragana(input = '') {
    if (isEmpty(input)) return false;
    return [...input].every(isCharHiragana);
}

/**
 * Test if `input` is [Katakana](https://en.wikipedia.org/wiki/Katakana)
 * @param  {String} [input=''] text
 * @return {Boolean} true if all [Katakana](https://en.wikipedia.org/wiki/Katakana)
 * @example
 * isKatakana('ゲーム')
 * // => true
 * isKatakana('ア')
 * // => false
 * isKatakana('A')
 * // => false
 * isKatakana('アア')
 * // => false
 */
function isKatakana(input = '') {
    if (isEmpty(input)) return false;
    return [...input].every(isCharKatakana);
}

/**
 * Tests a character. Returns true if the character is a CJK ideograph (kanji).
 * @param  {String} char character string to test
 * @return {Boolean}
 */
function isCharKanji(char = '') {
    return isCharInRange(char, KANJI_START, KANJI_END);
}

/**
 * Tests if `input` is [Kanji](https://en.wikipedia.org/wiki/Kanji) ([Japanese CJK ideographs](https://en.wikipedia.org/wiki/CJK_Unified_Ideographs))
 * @param  {String} [input=''] text
 * @return {Boolean} true if all [Kanji](https://en.wikipedia.org/wiki/Kanji)
 * @example
 * isKanji('刀')
 * // => true
 * isKanji('切腹')
 * // => true
 * isKanji('勢イ')
 * // => false
 * isKanji('アAア')
 * // => false
 * isKanji('🐸')
 * // => false
 */
function isKanji(input = '') {
    if (isEmpty(input)) return false;
    return [...input].every(isCharKanji);
}

/**
 * Test if `input` contains a mix of [Romaji](https://en.wikipedia.org/wiki/Romaji) *and* [Kana](https://en.wikipedia.org/wiki/Kana), defaults to pass through [Kanji](https://en.wikipedia.org/wiki/Kanji)
 * @param  {String} input text
 * @param  {Object} [options={ passKanji: true }] optional config to pass through kanji
 * @return {Boolean} true if mixed
 * @example
 * isMixed('Abアア'))
 * // => true
 * isMixed('オ腹A')) // ignores kanji by default
 * // => true
 * isMixed('オ腹A', { passKanji: false }))
 * // => false
 * isMixed('ab'))
 * // => false
 * isMixed('アア'))
 * // => false
 */
function isMixed(input = '', options = { passKanji: true }) {
    const chars = [...input];
    let hasKanji = false;
    if (!options.passKanji) {
    hasKanji = chars.some(isKanji);
    }
    return (chars.some(isHiragana) || chars.some(isKatakana)) && chars.some(isRomaji) && !hasKanji;
}

const isCharInitialLongDash = (char, index) => isCharLongDash(char) && index < 1;
const isCharInnerLongDash = (char, index) => isCharLongDash(char) && index > 0;
const isKanaAsSymbol = char => ['ヶ', 'ヵ'].includes(char);
const LONG_VOWELS = {
    a: 'ア',
    i: 'イ',
    u: 'ウ',
    e: 'エ',
    o: 'ウ'
};

// inject toRomaji to avoid circular dependency between toRomaji <-> katakanaToHiragana
function katakanaToHiragana(input = '', toRomaji, isDestinationRomaji) {
    let previousKana = '';

    return input.split('').reduce((hira, char, index) => {
    // Short circuit to avoid incorrect codeshift for 'ー' and '・'
    if (isCharSlashDot(char) || isCharInitialLongDash(char, index) || isKanaAsSymbol(char)) {
        return hira.concat(char);
        // Transform long vowels: 'オー' to 'オウ'
    } else if (previousKana && isCharInnerLongDash(char, index)) {
        // Transform previousKana back to romaji, and slice off the vowel
        const romaji = toRomaji(previousKana).slice(-1);
        // However, ensure 'オー' => 'オオ' => 'oo' if this is a transform on the way to romaji
        if (isCharKatakana(input[index - 1]) && romaji === 'o' && isDestinationRomaji) {
        return hira.concat('オ');
        }
        return hira.concat(LONG_VOWELS[romaji]);
    } else if (!isCharLongDash(char) && isCharKatakana(char)) {
        // Shift charcode.
        const code = char.charCodeAt(0) + (HIRAGANA_START - KATAKANA_START);
        const hiraChar = String.fromCharCode(code);
        previousKana = hiraChar;
        return hira.concat(hiraChar);
    }
    // Pass non katakana chars through
    previousKana = '';
    return hira.concat(char);
    }, []).join('');
}

let kanaToHepburnMap = null;

/* eslint-disable */
// prettier-ignore
const BASIC_ROMAJI = {
    ア: 'a', イ: 'i', ウ: 'u', エ: 'e', オ: 'o',
    カ: 'ka', キ: 'ki', ク: 'ku', ケ: 'ke', コ: 'ko',
    サ: 'sa', シ: 'shi', ス: 'su', セ: 'se', ソ: 'so',
    タ: 'ta', チ: 'chi', ツ: 'tsu', テ: 'te', ト: 'to',
    ナ: 'na', ニ: 'ni', ヌ: 'nu', ネ: 'ne', ノ: 'no',
    ハ: 'ha', ヒ: 'hi', フ: 'fu', ヘ: 'he', ホ: 'ho',
    マ: 'ma', ミ: 'mi', ム: 'mu', メ: 'me', モ: 'mo',
    ラ: 'ra', リ: 'ri', ル: 'ru', レ: 're', ロ: 'ro',
    ヤ: 'ya', ユ: 'yu', ヨ: 'yo',
    ワ: 'wa', ゐ: 'wi', ゑ: 'we', ヲ: 'wo',
    ン: 'n',
    ガ: 'ga', ギ: 'gi', グ: 'gu', ゲ: 'ge', ゴ: 'go',
    ザ: 'za', ジ: 'ji', ズ: 'zu', ゼ: 'ze', ゾ: 'zo',
    ダ: 'da', ヂ: 'ji', ヅ: 'zu', デ: 'de', ド: 'do',
    バ: 'ba', ビ: 'bi', ブ: 'bu', ベ: 'be', ボ: 'bo',
    パ: 'pa', ピ: 'pi', プ: 'pu', ペ: 'pe', ポ: 'po',
    ゔぁ: 'va', ゔぃ: 'vi', ゔ: 'vu', ゔぇ: 've', ゔぉ: 'vo'
};
/* eslint-enable  */

const SPECIAL_SYMBOLS$1 = {
    '。': '.',
    '、': ',',
    '：': ':',
    '・': '/',
    '！': '!',
    '？': '?',
    '〜': '~',
    'ー': '-',
    '「': '‘',
    '」': '’',
    '『': '“',
    '』': '”',
    '［': '[',
    '］': ']',
    '（': '(',
    '）': ')',
    '｛': '{',
    '｝': '}',
    '　': ' '
};

// ンイ -> n'i
const AMBIGUOUS_VOWELS = ['ア', 'イ', 'ウ', 'エ', 'オ', 'ヤ', 'ユ', 'ヨ'];
const SMALL_Y$1 = { ャ: 'ya', ュ: 'yu', ョ: 'yo' };
const SMALL_Y_EXTRA = { ぃ: 'yi', ぇ: 'ye' };
const SMALL_AIUEO = {
    ぁ: 'a',
    ぃ: 'i',
    ぅ: 'u',
    ぇ: 'e',
    ぉ: 'o'
};
const YOON_KANA = ['キ', 'ニ', 'ヒ', 'ミ', 'リ', 'ギ', 'ビ', 'ピ', 'ゔ', 'ク', 'フ'];
const YOON_EXCEPTIONS = {
    シ: 'sh',
    チ: 'ch',
    ジ: 'j',
    ヂ: 'j'
};
const SMALL_KANA = {
    っ: '',
    ャ: 'ya',
    ュ: 'yu',
    ョ: 'yo',
    ぁ: 'a',
    ぃ: 'i',
    ぅ: 'u',
    ぇ: 'e',
    ぉ: 'o'
};

// going with the intuitive (yet incorrect) solution where っヤ -> yya and っぃ -> ii
// in other words, just assume the sokuon could have been applied to anything
const SOKUON_WHITELIST = {
    b: 'b',
    c: 't',
    d: 'd',
    f: 'f',
    g: 'g',
    h: 'h',
    j: 'j',
    k: 'k',
    m: 'm',
    p: 'p',
    q: 'q',
    r: 'r',
    s: 's',
    t: 't',
    v: 'v',
    w: 'w',
    x: 'x',
    z: 'z'
};

function getKanaToHepburnTree() {
    if (kanaToHepburnMap == null) {
    kanaToHepburnMap = createKanaToHepburnMap();
    }
    return kanaToHepburnMap;
}

function getKanaToRomajiTree(fullOptions) {
    switch (fullOptions.romanization) {
    case ROMANIZATIONS.HEPBURN:
        return getKanaToHepburnTree();
    default:
        return {};
    }
}

function createKanaToHepburnMap() {
    const romajiTree = transform(BASIC_ROMAJI);

    const subtreeOf = string => getSubTreeOf(romajiTree, string);
    const setTrans = (string, transliteration) => {
    subtreeOf(string)[''] = transliteration;
    };

    Object.entries(SPECIAL_SYMBOLS$1).forEach(([jsymbol, symbol]) => {
    subtreeOf(jsymbol)[''] = symbol;
    });

    [...Object.entries(SMALL_Y$1), ...Object.entries(SMALL_AIUEO)].forEach(([roma, kana]) => {
    setTrans(roma, kana);
    });

    // キャ -> kya
    YOON_KANA.forEach(kana => {
    const firstRomajiChar = subtreeOf(kana)[''][0];
    Object.entries(SMALL_Y$1).forEach(([yKana, yRoma]) => {
        setTrans(kana + yKana, firstRomajiChar + yRoma);
    });
    // キぃ -> kyi
    Object.entries(SMALL_Y_EXTRA).forEach(([yKana, yRoma]) => {
        setTrans(kana + yKana, firstRomajiChar + yRoma);
    });
    });

    Object.entries(YOON_EXCEPTIONS).forEach(([kana, roma]) => {
    // ジャ -> ja
    Object.entries(SMALL_Y$1).forEach(([yKana, yRoma]) => {
        setTrans(kana + yKana, roma + yRoma[1]);
    });
    // ジぃ -> jyi, ジぇ -> je
    setTrans(`${kana}ぃ`, `${roma}yi`);
    setTrans(`${kana}ぇ`, `${roma}e`);
    });

    romajiTree['っ'] = resolveTsu(romajiTree);

    Object.entries(SMALL_KANA).forEach(([kana, roma]) => {
    setTrans(kana, roma);
    });

    AMBIGUOUS_VOWELS.forEach(kana => {
    setTrans(`ン${kana}`, `n'${subtreeOf(kana)['']}`);
    });

    // NOTE: could be re-enabled with an option?
    // // ンバ -> mbo
    // const LABIAL = [
    //   'バ', 'ビ', 'ブ', 'ベ', 'ボ',
    //   'パ', 'ピ', 'プ', 'ペ', 'ポ',
    //   'マ', 'ミ', 'ム', 'メ', 'モ',
    // ];
    // LABIAL.forEach((kana) => {
    //   setTrans(`ン${kana}`, `m${subtreeOf(kana)['']}`);
    // });

    return Object.freeze(JSON.parse(JSON.stringify(romajiTree)));
}

function resolveTsu(tree) {
    return Object.entries(tree).reduce((tsuTree, [key, value]) => {
    if (!key) {
        // we have reached the bottom of this branch
        const consonant = value.charAt(0);
        tsuTree[key] = Object.keys(SOKUON_WHITELIST).includes(consonant) ? SOKUON_WHITELIST[consonant] + value : value;
    } else {
        // more subtrees
        tsuTree[key] = resolveTsu(value);
    }
    return tsuTree;
    }, {});
}

/**
 * Convert kana to romaji
 * @param  {String} kana text input
 * @param  {DefaultOptions} [options=defaultOptions]
 * @return {String} converted text
 * @example
 * toRomaji('ヒラガナ　カタカナ')
 * // => 'hiragana katakana'
 * toRomaji('ゲーム　ゲーム')
 * // => 'ge-mu geemu'
 * toRomaji('ヒラガナ　カタカナ', { upcaseKatakana: true })
 * // => 'hiragana KATAKANA'
 * toRomaji('ツジギリ', { customRomajiMapping: { ジ: 'zi', ツ: 'tu', リ: 'li' } });
 * // => 'tuzigili'
 */
function toRomaji(input = '', options = {}) {
    const mergedOptions = mergeWithDefaultOptions(options);
    // just throw away the substring index information and just concatenate all the kana
    return splitIntoRomaji(input, mergedOptions).map(romajiToken => {
    const [start, end, romaji] = romajiToken;
    const makeUpperCase = options.upcaseKatakana && isKatakana(input.slice(start, end));
    return makeUpperCase ? romaji.toUpperCase() : romaji;
    }).join('');
}

let customMapping$1 = null;
function splitIntoRomaji(input, options) {
    let map = getKanaToRomajiTree(options);

    if (options.customRomajiMapping) {
    if (customMapping$1 == null) {
        customMapping$1 = mergeCustomMapping(map, options.customRomajiMapping);
    }
    map = customMapping$1;
    }

    return applyMapping(katakanaToHiragana(input, toRomaji, true), map, !options.IMEMode);
}

/**
 * Tests a character. Returns true if the character is considered English punctuation.
 * @param  {String} char character string to test
 * @return {Boolean}
 */
function isCharEnglishPunctuation(char = '') {
    if (isEmpty(char)) return false;
    return EN_PUNCTUATION_RANGES.some(([start, end]) => isCharInRange(char, start, end));
}

/**
 * Convert input to [Hiragana](https://en.wikipedia.org/wiki/Hiragana)
 * @param  {String} [input=''] text
 * @param  {DefaultOptions} [options=defaultOptions]
 * @return {String} converted text
 * @example
 * toHiragana('toukyou, オオサカ')
 * // => 'トウキョウ、　オオサカ'
 * toHiragana('only カナ', { passRomaji: true })
 * // => 'only カナ'
 * toHiragana('wi')
 * // => 'ウぃ'
 * toHiragana('wi', { useObsoleteKana: true })
 * // => 'ゐ'
 */
function toHiragana(input = '', options = {}) {
    const config = mergeWithDefaultOptions(options);
    if (config.passRomaji) {
    return katakanaToHiragana(input, toRomaji);
    }

    if (isMixed(input, { passKanji: true })) {
    const convertedKatakana = katakanaToHiragana(input, toRomaji);
    return toKana(convertedKatakana.toLowerCase(), config);
    }

    if (isRomaji(input) || isCharEnglishPunctuation(input)) {
    return toKana(input.toLowerCase(), config);
    }

    return katakanaToHiragana(input, toRomaji);
}
//List of all kanas
var kana = [
    ["ア", "イ", "ウ", "エ", "オ"], 
    ["カ", "キ", "ク", "ケ", "コ"], 
    ["サ", "シ", "ス", "セ", "ソ"], 
    ["タ", "チ", "ツ", "テ", "ト"], 
    ["ナ", "ニ", "ヌ", "ネ", "ノ"], 
    ["ハ", "ヒ", "フ", "ヘ", "ホ"], 
    ["マ", "ミ", "ム", "メ", "モ"], 
    ["ヤ", "ユ", "ヨ"], 
    ["ラ", "リ", "ル", "レ", "ロ"], 
    ["ワ", "ヲ", "ン"], 
    ["ガ", "ギ", "グ", "ゲ", "ゴ"], 
    ["ザ", "ジ", "ズ", "ゼ", "ゾ"], 
    ["ダ", "ヂ", "ヅ", "デ", "ド"], 
    ["バ", "ビ", "ブ", "ベ", "ボ"], 
    ["パ", "ピ", "プ", "ペ", "ポ"], 
    ["キャ", "キュ", "キョ"], 
    ["シャ", "シュ", "ショ"], 
    ["チャ", "チュ", "チョ"], 
    ["ニャ", "ニュ", "ニョ"], 
    ["ヒャ", "ヒュ", "ヒョ"], 
    ["ミャ", "ミュ", "ミョ"], 
    ["リャ", "リュ", "リョ"], 
    ["ギャ", "ギュ", "ギョ"], 
    ["ジャ", "ジュ", "ジョ"], 
    ["ヂャ", "ヂュ", "ヂョ"], 
    ["ビャ", "ビュ", "ビョ"], 
    ["ピャ", "ピュ", "ピョ"]
];

var t0;
var t1;

var startTimeCount = function startTimeCount(){
    t0 = performance.now();
}

var endTimeCount = function endTimeCount(){
    t1 = performance.now();
}

var copyArray = function makeCopyOfArray(source) {
    var array = [];
    source.forEach(function eachThroughSourceArray(item) {
    array.push(item);
    });
    return array;
};

var shuffleArray = function doShuffleOfArray(array) {
    var workingCopy = copyArray(array);
    var mappedArray = array.map(function () {
    var arraySize = workingCopy.length;
    var random = Math.floor(Math.random() * arraySize);
    return workingCopy.splice(random, 1)[0];
    });
    return mappedArray;
};

var isTouchable = function isTouchable() {
    return "ontouchstart" in window || navigator.MaxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
};

//window.$ = jquery;
var scores = [];
$(document).ready(function () {
    $(".start").find("input").prop("checked", false);
    $(".quiz-page").hide();
    $(".results").hide();
    $(".table-wrap").hide();
    $(".kana-header").hide();
    $(".kana-details").hide();
    $(".kana-study").hide();
    $("html,body").animate({
    scrollTop: 0
    }, 100);
}); // Start on first card function

var startOnFirstCard = function startOnFirstCard() {
    $(document).ready(function () {
    $(".card").find('input[type="text"]').eq(0).focus();
    $("html,body").animate({
        scrollTop: 0
    }, 100);
    });
}; // Append cards to quiz function


var appendCardstoQuiz = function appendCardstoQuiz(item) {
    $(".quiz").append("<label class=\"card\">\n    <div class=\"card__text\">".concat(item, "</div>\n    <form class=\"card__form\">\n    <input type=\"text\" class=\"card__field\" autocomplete=\"off\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\" maxlength=\"5\">\n    </form>\n    </label>"));
}; // Move to next function


var movePosition = function movePosition(index) {
    var position = index + 1;

    while ($(".card").eq(position).next().hasClass("correct")) {
    position += 1;
    }

    return position;
};

var moveToNext = function moveToNext(index) {
    var nextCard = $(".card").eq(index).next();
    var position = movePosition(index);
    var positionCard = $(".card").eq(position).next();

    if (nextCard.hasClass("correct")) {
    var focusPoint = positionCard.find('input[type="text"]');
    focusPoint.focus();

    if (focusPoint.length === 0) {
        $(".card").not(".correct").find('input[type="text"]').first().focus();
    }
    } else if (nextCard.length === 0) {
    $(".card").not(".correct").find('input[type="text"]').first().focus();
    } else {
    nextCard.find('input[type="text"]').focus();
    }
};

var renderCorrectCard = function renderCorrectCard(index) {
    $(".card").eq(index).removeClass("incorrect").addClass("correct");
    $(".card").eq(index).find("input").prop("disabled", true);
};

var renderIncorrectCard = function renderIncorrectCard(index) {
    $(".card").eq(index).removeClass("correct").addClass("incorrect");
    var incorrectVal = $(".card").eq(index).find('input[type="text"]').val();
    $(".card").eq(index).find('input[type="text"]').attr("placeholder", "".concat(incorrectVal));
    $(".card").eq(index).find('input[type="text"]').val("");
};

var keepScore = function keepScore(index) {
    var targetChar = $(".card__text").eq(index)[0].innerText;
    var objectExists = false;
    scores.forEach(function (item) {
    if (item.char === targetChar) {
        objectExists = true;
    }
    });

    if (objectExists) {
    scores.forEach(function (item) {
        if (item.char === targetChar) {
        var objIndex = scores.indexOf(item);
        scores[objIndex].incorrect += 1;
        }
    });
    } else {
    var obj = {};
    obj.char = targetChar;
    obj.incorrect = 1;
    scores.push(obj);
    }
}; // If correct function


var ifCorrect = function ifCorrect(item, index, userInput) {
    if (toHiragana(userInput) === item) {
    renderCorrectCard(index);
    moveToNext(index);
    } else {
    renderIncorrectCard(index);
    keepScore(index);
    moveToNext(index);
    }
}; // Highlight finish function


var highlightFinishButton = function highlightFinishButton() {
    var counter = 0;
    $.each($(".card"), function (index, item) {
    if ($(item).hasClass("correct")) {
        counter += 1;
    }

    if (counter === $(".card").length) {
        $(".focused-card").removeClass("focused-card");
        $(".end-quiz").focus();
    }
    });
}; // Check Answer function


var checkAnswer = function checkAnswer(item, index) {
    $("form").eq(index).on("submit", function (e) {
    e.preventDefault();
    var userInput = e.target[0].value;

    if (userInput === "") {
        console.log("nothing happens");
    } else {
        ifCorrect(item, index, userInput);
        highlightFinishButton();
    }
    });
};

var checkAnswerOnBlur = function checkAnswerOnBlur(item, index) {
    $("form").find("input").eq(index).on("blur", function (e) {
    e.preventDefault();
    var userInput = e.target.value;

    if (userInput === "") {
        console.log("nothing happens");
    } else {
        ifCorrect(item, index, userInput);
        highlightFinishButton();
    }
    });
}; // Deal Cards Function function


var dealCards = function dealCards(array) {
    var shuffledKana = shuffleArray(array);
    startOnFirstCard();
    shuffledKana.forEach(function (item, index) {
    appendCardstoQuiz(item);
    checkAnswer(item, index);
    checkAnswerOnBlur(item, index);
    });
}; // Start Page


var selectedCharacterIndices = []; // check/uncheck all options function

var checkAndUncheckAllSection = function checkAndUncheckAllSection(section, button) {
    $(".checkboxes.".concat(section)).find("input").prop("checked", false);

    if ($(".".concat(button)).find("input")[0].checked) {
    $(".checkboxes.".concat(section)).find("input").prop("checked", true);
    }
};

var checkAndUncheckAll = function checkAndUncheckAll() {
    $(".all-main, .all-combo, .all-dakuten").find("input").prop("checked", false).change();

    if ($(".all").find("input")[0].checked) {
    $(".all-main, .all-combo, .all-dakuten").find("input").prop("checked", true).change();
    }
};

var uncheckAllIfChildUnchecked = function uncheckAllIfChildUnchecked() {
    $(".options").find("input").on("change", function () {
    var parentInput = $(this).closest("div").prev("label").find("input");

    if (parentInput.prop("checked")) {
        parentInput.prop("checked", false);
    }
    });
};

uncheckAllIfChildUnchecked();
$(".all").find("input").change(function () {
    checkAndUncheckAll();
});

var listenAllCategory = function listenAllCategory(category) {
    $(".all-".concat(category)).find("input").change(function () {
    checkAndUncheckAllSection(category, "all-".concat(category));
    });
};

listenAllCategory("main");
listenAllCategory("dakuten");
listenAllCategory("combo");

var changeCheckboxBG = function changeCheckboxBG() {
    $(".select-box").find("input").on("change", function () {
    $.each($(".select-box").find("input"), function (index, item) {
        if ($(item).prop("checked")) {
        $(item).closest("label").addClass("check");
        } else {
        $(item).closest("label").removeClass("check");
        }
    });
    });
};

changeCheckboxBG();
var studyArray = []; // check checkboxes for indices

var scanCheckboxesForIndices = function scanCheckboxesForIndices() {
    $.each($(".options"), function (index, item) {
    if ($(item).find("input").prop("checked")) {
        selectedCharacterIndices = selectedCharacterIndices.concat(index);
    }
    });
}; // build study array function


var buildStudyArray = function buildStudyArray() {
    selectedCharacterIndices.forEach(function (item) {
    studyArray = studyArray.concat(kana[item]);
    });
}; // show quiz page function


var showQuizPage = function showQuizPage() {
    $(".quiz-page").show();
}; // disable start page function


var disableStartPage = function disableStartPage() {
    $(".press-start").prop("disabled", true);
    $(".options").find("input").prop("disabled", true);
    $(".all").find("input").prop("disabled", true);
};

var addComboClassToCard = function addComboClassToCard() {
    $.each($(".card__text"), function (index, item) {
    if (item.innerText.length >= 2) {
        $(item).addClass("card__text--combo");
    }
    });
};

$(".press-start").on("click", function () {
    var goodToGo = false;
    $.each($(".options").find("input"), function (index, item) {
    if ($(item).prop("checked")) {
        goodToGo = true;
    }
    });

    if (goodToGo) {
    startTimeCount();
    scanCheckboxesForIndices();
    buildStudyArray();
    showQuizPage();
    disableStartPage();
    dealCards(studyArray);
    $(".start").hide();
    } else {
    alert("Please make a selection to continue.");
    }

    addComboClassToCard();
}); // Quiz Page
// maintain scroll position

var maintainScroll = function maintainScroll() {
    if (isTouchable()) {
    return false;
    }

    return $(".quiz-page").on("focus", 'input[type="text"]', function (e) {
    var currentIndex = $(".card").find("input").index(e.target);
    var cardPosition = $(".card").eq(currentIndex).offset().top;
    window.scroll({
        top: cardPosition - 20,
        left: 0,
        behavior: "smooth"
    });
    });
};

maintainScroll();

var highlightFocusCard = function highlightFocusCard() {
    $(".quiz-page").on("focus", 'input[type="text"]', function () {
    $(".card").removeClass("focused-card");
    $(this).closest(".card").addClass("focused-card");
    });
};

highlightFocusCard(); // Results Page

var countAllCards = function countAllCards() {
    var cardNumber = 0;
    $(".card").each(function () {
    cardNumber += 1;
    });
    return cardNumber;
};

var countAllCorrect = function countAllCorrect() {
    var correctNumber = 0;
    $(".correct").each(function () {
    correctNumber += 1;
    });
    return correctNumber;
};

var calculateOverallCorrect = function calculateOverallCorrect() {
    var correctNumber = countAllCorrect();
    var cardNumber = countAllCards();
    var result = correctNumber / cardNumber * 100;
    $(".percent-results").append("<h3>Overall Correct: ".concat(correctNumber, "/").concat(cardNumber, " (").concat(result.toFixed(2), "%)</h3>"));
};

var calculateElapsedTime = function calculateElapsedTime() {
    var timeResult = (t1 - t0) / 1000;
    if (timeResult < 60){
        timeResult = Math.round(timeResult * 1000)/1000;
        $(".time-elapsed").append("<h3>Time: ".concat(timeResult, " seconds</h3>"));
    }
    else{
        var minutes = 0;
        while(timeResult >= 60){
            timeResult-=60;
            minutes++;
        }
        timeResult = Math.round(timeResult);
        $(".time-elapsed").append("<h3>Time: ".concat(minutes, " minutes ").concat(timeResult, " seconds</h3>"));
    }
};

var addValuesToFinalResultsTable = function addValuesToFinalResultsTable() {
    $(".kana-header").find("th").each(function (index, elem) {
    var hiragana = elem.innerText;
    var tableData = $(".kana-details").find("td").eq(index);
    scores.forEach(function (item) {
        if (item.char === hiragana) {
        tableData.append("\xD7: ".concat(item.incorrect));
        tableData.addClass("incorrect-result");
        }
    });

    if (tableData[0].innerText === "") {
        tableData[0].innerText = "◯";
        tableData.addClass("correct-result");
    }
    });
};

var scoreUnanswered = function scoreUnanswered() {
    var unanswered = $(".card").not(".correct").not(".incorrect");
    unanswered.each(function (index, item) {
    var targetChar = $(item).find(".card__text").text();
    var obj = {};
    obj.char = targetChar;
    obj.incorrect = "N/A";
    scores.push(obj);
    });
};

var ifNanDontShow = function ifNanDontShow(result, section) {
    if (Number.isNaN(result)) {
    section.prev("h2").hide();
    section.hide();
    }
};

var calculateCategoryResults = function calculateCategoryResults(section) {
    var correct = $(".".concat(section, "-table")).find(".correct-result:visible").length;
    var incorrect = $(".".concat(section, "-table")).find(".incorrect-result:visible").length;
    var total = correct + incorrect;
    var result = correct / total * 100;
    $(".".concat(section, "-score")).append("".concat(correct, "/").concat(total, " (").concat(result.toFixed(2), "%)"));
    var sectionScore = $(".".concat(section, "-score"));
    ifNanDontShow(result, sectionScore);
};

var showMatchingRowResults = function showMatchingRowResults() {
    selectedCharacterIndices.forEach(function (item) {
    $(".incorrect-details").find(".".concat(item)).show();
    $(".incorrect-details").find(".".concat(item)).closest(".table-wrap").show();
    });
}; // disable quiz page function


var disableQuizPage = function disableQuizPage() {
    $(".card").find("input").prop("disabled", true);
    $(".end-quiz").prop("disabled", true);
}; // End quiz function


var endQuiz = function endQuiz() {
    endTimeCount();
    disableQuizPage();
    $(".quiz-page").hide();
    $(window).scrollTop(0);
    $(".results").show();
    showMatchingRowResults();
    scoreUnanswered();
    calculateOverallCorrect();
    calculateElapsedTime();
    addValuesToFinalResultsTable();
    calculateCategoryResults("main");
    calculateCategoryResults("dakuten");
    calculateCategoryResults("combo");
    var windowScrollTop = $(window).scrollTop();
    window.scrollTo({
    top: windowScrollTop,
    behavior: "smooth"
    });
};

var checkUnansweredCardsBeforeEndQuiz = function checkUnansweredCardsBeforeEndQuiz() {
    var isEmpty = false;
    $.each($(".card").find('input[type="text"]'), function (index, item) {
    if (item.value === "") {
        isEmpty = true;
    }
    });

    if (isEmpty) {
    var promptWindow = window.confirm("You still have unanswered cards. Are you sure you want to end the quiz?");

    if (promptWindow) {
        endQuiz();
    }
    } else {
    endQuiz();
    }
};

$(".end-quiz").on("click", function () {
    checkUnansweredCardsBeforeEndQuiz();
});
$(".restart").on("click", function () {
    window.location.reload();
});

