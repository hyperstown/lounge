from django.urls import path

from .views import *

urlpatterns = [
    path('hiragana/', HiraganaPracticeView.as_view(), name='hiragana'),
    path('katakana/', KatakanaPracticeView.as_view(), name='katakana'),
]