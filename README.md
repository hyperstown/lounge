temp passwd to local db: a

# API

## Game playtimes:

### Lutris endpoints:

### 1. Lutris games form database list:

Endpoint: `/api/games/list-lutris/`

Description - list all games from lutris database (gametimes.db)

Allowed methods: `GET`, `POST`, `PUT`

Example `POST` data:

```json
{
    "name": "Clannad",
    "platform": "Linux",
    "runner": "linux",
    "lastplayed": 1621434186,
    "installed_at": 1617398261,
    "year": "2004",
    "playtime": 60.325
}
```
or a list

```json
[
    {
        "name": "Deus Ex",
        "platform": "Windows",
        "runner": "wine",
        "lastplayed": 1618254567,
        "installed_at": 1617573392,
        "year": "2011",
        "playtime": 23.433
    },
    {
        "name": "The Witcher 3",
        "platform": "Windows",
        "runner": "wine",
        "lastplayed": 1621434186,
        "installed_at": 1617398261,
        "year": "2015",
        "playtime": 120.325
    }
]
```
(List will be appended)

`PUT` accepts only lists. Example above.

### 2. Lutris games list backup:

Endpoint: `/api/games/list-lutris-backup/`

Description - backup of lutris games in local db.

Allowed methods: `GET`

### 3. Sync Lutris games to backup:

Endpoint: `/api/games/sync-lutris/`

Description - Creates backup of lutris games in local database

Allowed methods: `GET`

Accepted query params: `?fake=true` - returns backup data

### GOG endpoints:

### 1. GOG games list:

Endpoint: `api/games/list-gog/`

Description - list all gog games. (Local database)

Allowed methods: `GET`, `POST`, `PUT`

Example `POST` data:

Example `POST` data:

```json
{
    "name": "The Witcher 3",
    "year": "2015",
    "playtime": 120.325
}
```
or with unix timestamp

```json
{
    "name": "Deus Ex",
    "year": "2011",
    "lastplayed": 1621434186,
    "installed_at": 1621434186,
    "playtime": 23.433
}
```
or a list

```json
[
    {
        "name": "Deus Ex",
        "year": "2011",
        "playtime": 23.433
    },
    {
        "name": "The Witcher 3",
        "year": "2015",
        "playtime": 120.325
    }
]
```
(List will be appended)

`PUT` accepts only lists. Example above.

### 2. Lutris games list backup:

Endpoint: `/api/games/list-gog-backup/`

Description - backup of gog games in local db.

Allowed methods: `GET`

### 3. Sync GOG games to backup:

Endpoint: `/api/games/sync-gog/`

Description - Creates backup of gog games table in the same local database

Allowed methods: `GET`

Accepted query params: `?fake=true` - returns backup data


### General game list endpoints:

Endpoint: `/api/games/sync-list/`

Description - Fetches all games from Lutris list, GOG list, merges them together and update main list.

If element form merged list doesn't exist creates new.

If element exist overwrites it's values.

Allowed methods: `GET`

Accepted query params: `?save=false` - returns merged data yet to be save.
