#!/bin/bash

sqlite3 gog.db "SELECT DISTINCT trim(trim(GamePieces.value,'{\"\"title\"\":\"\"'),'\"\"}'), GameTimes.minutesInGame FROM GameTimes
INNER JOIN GamePieces ON GameTimes.releaseKey=GamePieces.releaseKey
WHERE gamePieceTypeId = 346 --lol
--WHERE GamePieces.value LIKE '{\"title\":\"%\"}' 
--GROUP BY GamePieces.releaseKey
ORDER BY GameTimes.minutesInGame DESC, GamePieces.value" | awk -F'|' '
   # sqlite output line - pick up fields and store in arrays
   { name[++i]=$1; playtime[i]=$2; }

   END {
      printf "[\n";
      for(j=1;j<=i;j++){
         printf "  {\n"
         printf "    |name|:|%s|,\n",name[j]
         printf "    |playtime|:%f\n",(playtime[j]/60)
         closing="  },\n"
         if(j==i){closing="  }\n"}
         printf closing;
      }
      printf "]\n";
   }' | tr '|' '"'