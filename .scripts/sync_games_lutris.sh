#!/bin/bash

sqlite3 pga.db "SELECT id, name, platform, runner, lastplayed, installed_at, year, playtime FROM games" | awk -F'|' '
   # sqlite output line - pick up fields and store in arrays
   { id[++i]=$1; name[i]=$2; platform[i]=$3; runner[i]=$4; lastplayed[i]=$5; installed_at[i]=$6; year[i]=$7; playtime[i]=$8 }

   END {
      printf "[\n";
      for(j=1;j<=i;j++){
         printf "  {\n"
         printf "    |id|:%d,\n",id[j]
         printf "    |name|:|%s|,\n",name[j]
         printf "    |runner|:|%s|,\n",runner[j]
         printf "    |lastplayed|:%d,\n",lastplayed[j]
         printf "    |installed_at|:%d,\n",installed_at[j]
         printf "    |year|:|%s|,\n",year[j]
         printf "    |playtime|:%f\n",playtime[j]
         closing="  },\n"
         if(j==i){closing="  }\n"}
         printf closing;
      }
      printf "]\n";
   }' | tr '|' '"'