from datetime import datetime

def copy_fields(instance_src, instance_dest, using_src=None, using_dest=None):
    """ Copy all values and fields into another model"""

    if using_src:
        objects_src = instance_src.objects.using(using_src).all()
    else:
        objects_src = instance_src._default_manager.all()

    if using_dest:
        instance_dest.objects.using(using_dest).all().delete()
    else:
        instance_dest._default_manager.all().delete()

    for object_src in objects_src:
        instance = instance_dest()
        for field in object_src._meta.fields:
            # if field.primary_key == True:
            #     continue  # don't want to clone the PK
            setattr(instance, field.name, getattr(object_src, field.name))
        # TODO add last sync
        instance.save()

def str_full_time(timestamp):
    if isinstance(timestamp, int):
        return datetime.fromtimestamp(timestamp).strftime("%Y-%m-%dT%H:%M:%S")
    elif isinstance(timestamp, str):
        return timestamp
    return None
