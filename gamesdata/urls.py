from django.urls import path

from .views import *

urlpatterns = [
    path('', GamesDetailsListView.as_view(), name='game-details'),
]