# Generated by Django 3.1.7 on 2021-05-23 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamesdata', '0003_gamedetailslutris_localgamedetailslutris'),
    ]

    operations = [
        migrations.AlterField(
            model_name='localgamedetailslutris',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
