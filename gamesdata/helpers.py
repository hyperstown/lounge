import string
from operator import itemgetter
from lounge.helpers import str_full_time

# https://www.reddit.com/r/gog/comments/ek3vtz/dev_gog_galaxy_20_get_list_of_gameid_of_installed/ 


def compare_names(n1, n2):
    ''' remove non ascii characters and compare '''
    n1 = ''.join(filter(lambda x: x in set(string.printable), n1)).replace(' ', '-').lower()
    n2 = ''.join(filter(lambda x: x in set(string.printable), n2)).replace(' ', '-').lower()
    if n1 == n2:
        return True
    return False
    

def does_dictlist_contain(dictlist, key, param):
    ''' If dict contain key with value == param returns index '''
    return next((i for i, item in enumerate(dictlist) if compare_names(item[key], param)), None)
  

def merge_playtime(dict_list1, dict_list2):
    
    if dict_list1 and dict_list2:
        if len(dict_list1[0].keys()) < len(dict_list2[0].keys()):
            dict_list1, dict_list2 = dict_list2, dict_list1

        if not dict_list1[0].get('id'):
            for i, _ in enumerate(dict_list1):
                dict_list1[i]['id'] = i

    
    dict_list1 = sorted(dict_list1, key=itemgetter('id'))
    
    for _dict in dict_list2:
        index = does_dictlist_contain(dict_list1, 'name', _dict['name'])
        if index is not None:
            dict_list1[index]['playtime']+=_dict['playtime']
        else:
            _dict['id'] = dict_list1[-1]['id'] + 1
            dict_list1.append(_dict)

    
    return dict_list1

def update_all(dict_list1, dict_list2):

    if dict_list1 and dict_list2:
        if len(dict_list1[0].keys()) < len(dict_list2[0].keys()):
            dict_list1, dict_list2 = dict_list2, dict_list1

        if not dict_list1[0].get('id'):
            for i, _ in enumerate(dict_list1):
                dict_list1[i]['id'] = i

    skip_first_element = False
    if not dict_list1:
        dict_list1.append(
            {
                "id": 0,
                "name": "placeholder"
            }
        )
        skip_first_element = True

    dict_list1 = sorted(dict_list1, key=itemgetter('id'))
    
    for _dict in dict_list2:
        index = does_dictlist_contain(dict_list1, 'name', _dict['name'])
        if index is not None:
            dict_list1[index]['playtime'] = _dict.get('playtime')
            dict_list1[index]['first_time_played'] = str_full_time(_dict.get('installed_at'))
            dict_list1[index]['last_played'] = str_full_time(_dict.get('lastplayed'))
            dict_list1[index]['release_year'] = _dict.get('year')
        else:
            new_entry = {}
            new_entry['id'] = dict_list1[-1]['id'] + 1
            new_entry['name'] = _dict.get('name')
            new_entry['playtime'] = _dict.get('playtime')
            new_entry['first_time_played'] = str_full_time(_dict.get('installed_at'))
            new_entry['last_played'] = str_full_time(_dict.get('lastplayed'))
            new_entry['release_year'] = _dict.get('year')
            dict_list1.append(new_entry)

    if skip_first_element:
        return dict_list1[1:]
    return dict_list1