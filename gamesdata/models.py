from django.db import models

# Combined

class LastSync(models.Model):
    last_sync = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.last_sync

class GameDetails(models.Model):
    """Game details model"""

    name = models.CharField(max_length=400)
    playtime = models.FloatField(blank=True, null=True)
    launcher = models.CharField(max_length=500, blank=True, null=True)
    first_time_played = models.DateTimeField(blank=True, null=True)
    last_played = models.DateTimeField(blank=True, null=True)
    release_year = models.CharField(max_length=400, blank=True, null=True)
    icon = models.FileField(blank=True, null=True)
    banner = models.FileField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def playtime_h(self):
        minutes_percentage = self.playtime - int(self.playtime)
        return f"{str(int(self.playtime))}h {str(int(60 * minutes_percentage))}m"

    class Meta:
        verbose_name_plural = "Games Details"

# GOG

class LocalGameDetailsGOG(models.Model):
    """Local Game details model"""
    models.AutoField()
    name = models.TextField()
    platform = models.TextField(null=True, default='Windows')
    lastplayed = models.IntegerField(null=True, blank=True)
    installed_at = models.IntegerField(null=True, blank=True)
    year = models.CharField(max_length=4, null=True, blank=True)
    playtime = models.FloatField(null=True)

    def __str__(self):
        return self.name

class BackupGameDetailsGOG(models.Model):
    """Local Game details model"""
    models.AutoField()
    name = models.TextField()
    platform = models.TextField(null=True, default='Windows')
    lastplayed = models.IntegerField(null=True, blank=True)
    installed_at = models.IntegerField(null=True, blank=True)
    year = models.CharField(max_length=4, null=True, blank=True)
    playtime = models.FloatField(null=True)

    def __str__(self):
        return self.name

# Lutris

class LocalGameDetailsLutris(models.Model):
    """Local Game details model"""

    name = models.TextField()
    platform = models.TextField()
    runner = models.TextField()
    lastplayed = models.IntegerField(null=True, blank=True)
    installed_at = models.IntegerField(null=True, blank=True)
    year = models.CharField(max_length=4, null=True, blank=True)
    playtime = models.FloatField(null=True)

    def __str__(self):
        return self.name

class GameDetailsLutrisManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().using('lutris_db')

class GameDetailsLutris(models.Model):
    """Game details model"""

    name = models.TextField()
    platform = models.TextField()
    runner = models.TextField()
    lastplayed = models.IntegerField(null=True, blank=True)
    installed_at = models.IntegerField(null=True, blank=True)
    year = models.CharField(max_length=4, null=True, blank=True)
    playtime = models.FloatField(null=True)

    objects = GameDetailsLutrisManager()

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'games'
        verbose_name = 'Game Details Lutris'
        verbose_name_plural = 'Games Details Lutris'