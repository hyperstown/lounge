from django.urls import path
#from rest_framework import routers
from .views import *


# router = routers.DefaultRouter()
# router.register(r"test", TestViewSet)

urlpatterns = [
    path('list/', GameDetailsAPIView.as_view()),
    path('list-lutris/', GameDetailsLutrisAPIView.as_view()),
    path('list-gog/', GameDetailsGOGAPIView.as_view()),
    path('list-lutris-backup/', BackupGameDetailsLutrisAPIView.as_view()),
    path('list-gog-backup/', BackupGameDetailsGOGAPIView.as_view()),
    path('sync-lutris/', CloneLutrisGameTimesToLocalDataBaseAPIView.as_view()),
    path('sync-gog/', CloneGOGGameTimesToLocalDataBaseAPIView.as_view()),
    path('sync-list/', UpdateAllGameDetailsAPIView.as_view()),
]

# urlpatterns += router.urls
