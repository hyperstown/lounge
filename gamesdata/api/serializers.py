from rest_framework import serializers
from ..models import *

class GameDetailsSerializer(serializers.ModelSerializer):
    ''' GameDetails Serializer '''
    playtime_h = serializers.CharField(read_only=True, required=False)
    class Meta:
        model = GameDetails
        fields = '__all__'

class CloneGameDetailsSerializer(serializers.ModelSerializer):
    ''' GameDetails Serializer '''
    class Meta:
        model = GameDetails
        fields = '__all__'

# Lutris

class BackupGameDetailsLutrisSerializer(serializers.ModelSerializer):
    ''' GameDetails Serializer '''

    class Meta:
        model = LocalGameDetailsLutris
        fields = '__all__'

class GameDetailsLutrisSerializer(serializers.ModelSerializer):
    ''' GameDetails Serializer '''

    # # Overwrite create if db is not set in manager.
    
    # def create(self, validated_data):
    #     return GameDetailsLutris.objects.using('lutris_db').create(**validated_data)

    class Meta:
        model = GameDetailsLutris
        fields = '__all__'


# GOG

class GameDetailsGOGSerializer(serializers.ModelSerializer):
    ''' GameDetails Serializer '''

    class Meta:
        model = LocalGameDetailsGOG
        fields = '__all__'

class BackupGameDetailsGOGSerializer(serializers.ModelSerializer):
    ''' GameDetails Serializer '''

    class Meta:
        model = BackupGameDetailsGOG
        fields = '__all__'
