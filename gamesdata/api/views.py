from django.db import connection
from django.conf import settings
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.generics import ListCreateAPIView, ListAPIView

from lounge.helpers import copy_fields
from ..models import *
from ..helpers import *
from .serializers import *

class GameDetailsAPIView(ListCreateAPIView):

    queryset = GameDetails.objects.all()
    serializer_class = GameDetailsSerializer
    #permission_classes = [IsAdminUser]
    
    def post(self, request):
        if isinstance(request.data, list) and request.data:
            headers = {}
            response_data = []
            for data in request.data:
                serializer = self.get_serializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer)
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return self.create(request)

    def put(self, request):
        if isinstance(request.data, list) and request.data:
            headers = {}
            response_data = []
            self.get_queryset().delete()
            with connection.cursor() as c:
                if settings.DATABASES['default'].get('ENGINE') == 'django.db.backends.sqlite3':
                    # For SQLite
                    c.execute(f"DELETE FROM `sqlite_sequence` WHERE `name` = '{GameDetails.objects.model._meta.db_table}';")
                else:
                    # For MySQL. Needs tests and cases for postgres etc
                    c.execute(f'ALTER TABLE {GameDetails.objects.model._meta.db_table} AUTO_INCREMENT = 1;')

            for data in request.data:
                serializer = self.get_serializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer)
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                {"message": f"PUT accept only JSON list. Got {type(request.data)}"},
                status=status.HTTP_400_BAD_REQUEST
            )

# Lutris

class BackupGameDetailsLutrisAPIView(ListAPIView):
    queryset = LocalGameDetailsLutris.objects.all()
    serializer_class = BackupGameDetailsLutrisSerializer

class GameDetailsLutrisAPIView(ListCreateAPIView):

    queryset = GameDetailsLutris.objects.using('lutris_db').all()
    serializer_class = GameDetailsLutrisSerializer

    def get(self, request):
        
        if not settings.DATABASES.get('lutris_db'):
            return Response(
                {"message": f"Cannot connect to lutris db. Current path '{settings.LUTRIS_PATH}'"},
                status=status.HTTP_503_SERVICE_UNAVAILABLE
            )
        return super().get(request)
    
    def post(self, request):
        if isinstance(request.data, list) and request.data:
            headers = {}
            response_data = []
            for data in request.data:
                serializer = GameDetailsLutrisSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer) # DRF does not support .save(using='lutris_db') ATM
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return super().post(request)

    def put(self, request):
        if isinstance(request.data, list) and request.data:
            headers = {}
            response_data = []
            self.get_queryset().delete()
            for data in request.data:
                serializer = GameDetailsLutrisSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer) # DRF does not support .save(using='lutris_db') ATM
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_200_OK, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                {"message": f"PUT accept only JSON list. Got {type(request.data)}"},
                status=status.HTTP_400_BAD_REQUEST
            )


# GOG

class GameDetailsGOGAPIView(ListCreateAPIView):

    queryset = LocalGameDetailsGOG.objects.all()
    serializer_class = GameDetailsGOGSerializer
    
    def post(self, request):
        if isinstance(request.data, list) and request.data:
            headers = {}
            response_data = []
            for data in request.data:
                serializer = GameDetailsGOGSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer) # DRF does not support .save(using='lutris_db') ATM
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return super().post(request)

    def put(self, request):
        if isinstance(request.data, list) and request.data:
            headers = {}
            response_data = []
            if not request.query_params.get('append', 'false') == 'true':
                self.get_queryset().delete()
                with connection.cursor() as c:
                    if settings.DATABASES['default'].get('ENGINE') == 'django.db.backends.sqlite3':
                        # For SQLite
                        c.execute(f"DELETE FROM `sqlite_sequence` WHERE `name` = '{LocalGameDetailsGOG.objects.model._meta.db_table}';")
                    else:
                        # For MySQL. Needs tests and cases for postgres etc
                        c.execute(f'ALTER TABLE {LocalGameDetailsGOG.objects.model._meta.db_table} AUTO_INCREMENT = 1;')
            for data in request.data:
                serializer = GameDetailsGOGSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer) # DRF does not support .save(using='lutris_db') ATM
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_200_OK, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":f"Invalid data. Data must be list. Got {type(request.data)}"}, status=status.HTTP_400_BAD_REQUEST)

class BackupGameDetailsGOGAPIView(ListAPIView):
    queryset = BackupGameDetailsGOG.objects.all()
    serializer_class = GameDetailsGOGSerializer


# Clone

class CloneLutrisGameTimesToLocalDataBaseAPIView(APIView):

    def get(self, request):
        
        if not settings.DATABASES.get('lutris_db'):
            return Response(
                {"message": f"Cannot connect to lutris db. Current path '{settings.LUTRIS_PATH}'"},
                status=status.HTTP_503_SERVICE_UNAVAILABLE
            )
        
        if not request.query_params.get('fake', 'false') == 'true':
            copy_fields(GameDetailsLutris, LocalGameDetailsLutris)
        game_details = LocalGameDetailsLutris.objects.all()
        serializer = GameDetailsLutrisSerializer(game_details, many=True)
        return Response(
            {
                "message": "OK!",
                "response_data": serializer.data
            }
        )

class CloneGOGGameTimesToLocalDataBaseAPIView(APIView):

    def get(self, request):
        
        copy_fields(LocalGameDetailsGOG, BackupGameDetailsGOG)
        game_details = BackupGameDetailsGOG.objects.all()
        serializer = GameDetailsGOGSerializer(game_details, many=True)
        return Response(
            {
                "message": "OK!",
                "response_data": serializer.data
            }
        )

class UpdateAllGameDetailsAPIView(APIView):

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    def get(self, request):
        # get gog
        game_details_gog = LocalGameDetailsGOG.objects.all()
        serialized_data_gog = GameDetailsGOGSerializer(game_details_gog, many=True).data

        # get lutris
        game_details_lutris = GameDetailsLutris.objects.using('lutris_db').all()
        serialized_data_lutris = GameDetailsLutrisSerializer(game_details_lutris, many=True).data
        
        # merge gog and lutris
        merged_launchers_data = merge_playtime(serialized_data_lutris, serialized_data_gog)
        
        # get local full list
        game_details_local = GameDetails.objects.all()
        serialized_data_local = CloneGameDetailsSerializer(game_details_local, many=True).data

        # merge local full list
        updated_list = update_all(serialized_data_local, merged_launchers_data)

        if request.query_params.get('save', 'true') == 'false':
            return Response(
            {
                "message": "OK!",
                "response_data": updated_list
            }
        )

        if isinstance(updated_list, list) and request.data:
            headers = {}
            response_data = []
            GameDetails.objects.all().delete()
            with connection.cursor() as c:
                if settings.DATABASES['default'].get('ENGINE') == 'django.db.backends.sqlite3':
                    # For SQLite
                    c.execute(f"DELETE FROM `sqlite_sequence` WHERE `name` = '{GameDetails.objects.model._meta.db_table}';")
                else:
                    # For MySQL. Needs tests and cases for postgres etc
                    c.execute(f'ALTER TABLE {GameDetails.objects.model._meta.db_table} AUTO_INCREMENT = 1;')

            for data in updated_list:
                serializer = CloneGameDetailsSerializer(data=data)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                headers = self.get_success_headers(serializer.data)
                response_data.append(serializer.data)
            return Response(response_data, status=status.HTTP_200_OK, headers=headers)
        elif isinstance(request.data, list) and not request.data:
            return Response({"message":"Invalid data. Recivied Empty list"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                {"message": f"PUT accept only JSON list. Got {type(updated_list)}"},
                status=status.HTTP_400_BAD_REQUEST
            )

