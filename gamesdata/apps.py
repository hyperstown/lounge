from django.apps import AppConfig


class GamesdataConfig(AppConfig):
    name = 'gamesdata'
